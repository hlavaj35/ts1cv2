package lab03;


import cz.cvut.fel.ts1.Calculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculatorTest {
    Calculator calculator = new Calculator();

    @Test
    public void assertAdd(){
        Assertions.assertEquals(4,calculator.add(1,3));
    }

    @Test
    public void assertSubtract(){
        Assertions.assertEquals(0,calculator.subtract(3,3));
    }

    @Test
    public void assertMultiply(){
        Assertions.assertEquals(3,calculator.multiply(1,3));
    }

    @Test
    public void assertDivide(){
        Assertions.assertEquals(1,calculator.divide(3,3));
        Assertions.assertEquals(0,calculator.divide(2,0));

    }



}
